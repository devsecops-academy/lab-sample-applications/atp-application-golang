package main

import (
	"database/sql"
	"os"
)

import _ "github.com/go-sql-driver/mysql"

func getData() ([]Customer, error) {
	var customers []Customer
	db, err := sql.Open("mysql", os.Getenv("DB_CONNECTION_STRING"))

	if err != nil {
		return nil, err
	}

	defer db.Close()

	if err := db.Ping(); err != nil {
		return nil, err
	}

	rows, err := db.Query("SELECT first_name, last_name, email FROM customer;")

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var customer Customer

		if err := rows.Scan(&customer.FirstName, &customer.LastName, &customer.Email); err != nil {
			return nil, err
		}

		customers = append(customers, customer)
	}

	return customers, nil
}
