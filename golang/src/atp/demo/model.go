package main

type Customer struct {
	FirstName string
	LastName string
	Email string
}

type PageData struct {
	Customers []Customer
}