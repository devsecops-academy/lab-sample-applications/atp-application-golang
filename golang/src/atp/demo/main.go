package main

import (
	"fmt"
	"log"
	"net/http"
	"html/template"
)

func main() {
	http.HandleFunc("/", RenderHomePage)
	log.Print("Listening on port 8016.")
	log.Fatal(http.ListenAndServe(":8016", nil))
}

func RenderHomePage(w http.ResponseWriter, r *http.Request) {
	template := template.Must(template.ParseFiles("home.html"))

	data, err := getData()

	if err != nil {
		fmt.Fprintf(w, "Error retrieving data.")
		return
	}

	model := PageData {
		Customers: data,
	}

	template.Execute(w, model)
	w.Header().Set("Content-Type", "text/html")
}