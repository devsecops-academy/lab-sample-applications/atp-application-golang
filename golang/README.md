# Running Locally

```
docker run -it --rm --mount type=bind,source="$(pwd)",target=/go -p 8016:8016 --link db -e DB_CONNECTION_STRING="82618201:animism-sort-galvanic-angina-camp@(db)/demo" golang:alpine
```

```
go get ./...
go install atp/demo
./bin/demo
```